import { useState } from "react";

function ListContainer({ list, setList }) {
  return (
    <div className="list">
      {list.map((itemData, index) => {
        return (
          <div key={itemData.data + index} className="list-item">
            <input
              type="checkbox"
              name="item-input"
              id={itemData.data + index}
              checked={itemData.isChecked}
              onChange={() => {
                const updatedList = list.map((listData, listIndex) => {
                  if (index == listIndex) {
                    return { ...listData, isChecked: !listData.isChecked };
                  } else {
                    return listData;
                  }
                });
                setList(updatedList);
              }}
            />
            <label htmlFor={itemData.data + index}>{itemData.data}</label>
          </div>
        );
      })}
    </div>
  );
}

function App() {
  const [listOne, setListOne] = useState([
    {
      data: "HTML",
      isChecked: false,
    },
    {
      data: "CSS",
      isChecked: false,
    },
    {
      data: "JS",
      isChecked: false,
    },
    {
      data: "TS",
      isChecked: false,
    },
  ]);

  const [listTwo, setListTwo] = useState([
    {
      data: "React",
      isChecked: false,
    },
    {
      data: "Angular",
      isChecked: false,
    },
    {
      data: "Vue",
      isChecked: false,
    },
    {
      data: "Svelte",
      isChecked: false,
    },
  ]);

  let countOfListOneCheckedItems = listOne.reduce((accumulator, listData) => {
    return listData.isChecked ? accumulator + 1 : accumulator;
  }, 0);

  let countOfListTwoCheckedItems = listTwo.reduce((accumulator, listData) => {
    return listData.isChecked ? accumulator + 1 : accumulator;
  }, 0);

  function handleMoveLeft() {
    let updatedListTwo = listTwo.filter((listData) => !listData.isChecked);
    let itemsToAddInListOne = listTwo
      .filter((listData) => listData.isChecked)
      .map((listData) => {
        return { ...listData, isChecked: false };
      });
    setListOne([...listOne].concat(itemsToAddInListOne));
    setListTwo(updatedListTwo);
  }

  function handleMoveRight() {
    let updatedListOne = listOne.filter((listData) => !listData.isChecked);
    let itemsToAddInListTwo = listOne
      .filter((listData) => listData.isChecked)
      .map((listData) => {
        return { ...listData, isChecked: false };
      });
    setListOne(updatedListOne);
    setListTwo([...listTwo].concat(itemsToAddInListTwo));
  }

  function handleMoveAllLeft() {
    let updatedListTwo = listTwo.map((listData) => {
      return { ...listData, isChecked: false };
    });
    let updatedListOne = listOne.concat(updatedListTwo);
    setListOne(updatedListOne);
    setListTwo([]);
  }

  function handleMoveAllRight() {
    let updatedListOne = listOne.map((listData) => {
      return { ...listData, isChecked: false };
    });
    let updatedListTwo = listTwo.concat(updatedListOne);
    setListOne([]);
    setListTwo(updatedListTwo);
  }

  return (
    <div className="app">
      <h1>Transfer List</h1>
      <div className="app-container">
        <ListContainer list={listOne} setList={setListOne} />
        <div className="buttons">
          <button
            onClick={handleMoveAllLeft}
            disabled={listTwo.length ? false : true}
          >
            &lt;&lt;
          </button>
          <button
            onClick={handleMoveLeft}
            disabled={countOfListTwoCheckedItems ? false : true}
          >
            &lt;
          </button>
          <button
            onClick={handleMoveRight}
            disabled={countOfListOneCheckedItems ? false : true}
          >
            &gt;
          </button>
          <button
            onClick={handleMoveAllRight}
            disabled={listOne.length ? false : true}
          >
            &gt;&gt;
          </button>
        </div>
        <ListContainer list={listTwo} setList={setListTwo} />
      </div>
    </div>
  );
}

export default App;
